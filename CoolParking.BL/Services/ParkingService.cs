﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Timers;
using System.Transactions;

namespace CoolParking.BL.Services
{
	public class ParkingService : IParkingService, IDisposable
	{
		public ParkingService()
		{
			vehicles = new List<Vehicle>();
			parking = new Parking();
			transactions = new List<TransactionInfo>();
			logService = new LogService();
			timerService = new TimerService();
		}

		private static LogService logService;
		private static TimerService timerService;

		private Parking parking;
		private List<Vehicle> vehicles;
		private List<TransactionInfo> transactions;

		int CountFreeParkingSpace => Settings.ParkingSpace - vehicles.Count;
		public Vehicle GetVehicle(string id)=> parking.Vehicles.Find(c => c.Id == id);

		public void AddVehicle(Vehicle vehicle)
		{
			if(vehicle == null)
			{
				throw new ArgumentNullException(nameof(vehicle));
			}
			if(CountFreeParkingSpace == 0)
			{
				throw new InvalidOperationException("Немає вільних місць.");
			}
			//vehicles.Add(vehicle);
			parking.Vehicles.Add(vehicle);
			timerService.Start();
		}

		public void Dispose()
		{
			throw new NotImplementedException();
		}

		public decimal GetBalance()
		{
			return parking.Balance;
		}

		public int GetCapacity()
		{
			return Settings.ParkingSpace;
		}

		public int GetFreePlaces()
		{
			return CountFreeParkingSpace;
		}

		public TransactionInfo[] GetLastParkingTransactions()
		{
			DateTime now = DateTime.Now;
			foreach (var item in Enumerable.Reverse(transactions))
			{
				if (now - item.TimeOfTransaction > Settings.PeriodOfLogging)
				{
					break;
				}
				return null;
			}
			return transactions.ToArray();
		}

		public ReadOnlyCollection<Vehicle> GetVehicles()
		{
			return new ReadOnlyCollection<Vehicle>(parking.Vehicles);
		}

		public string ReadFromLog()
		{
			return logService.Read();
		}

		public void RemoveVehicle(string vehicleId)
		{
			Vehicle vehicle = GetVehicle(vehicleId);
			int index = parking.Vehicles.IndexOf(vehicle);

			if(vehicle == null)
			{
				throw new ArgumentNullException(nameof(vehicle));
			}
			if (index >= 0)
			{
				if (vehicle.Balance < 0)
				{
					throw new ArgumentOutOfRangeException("Баланс менше нуля.");
				}
				parking.Vehicles.RemoveAt(index);
				timerService.Stop();
			}
		}
		public void TopUpVehicle(string vehicleId, decimal sum)
		{
			Vehicle vehicle = GetVehicle(vehicleId);
			if (sum<=0)
			{
				throw new ArgumentOutOfRangeException(nameof(sum), "Сума поповнення повинна бути більша нуля.");
			}
			if(vehicle != null)
			{
				vehicle.Balance += sum;
			}
		}
	}
}